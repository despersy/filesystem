package main

type FileSystem struct {
	Clusters []Cluster
	File     []File
}

type File struct {
	uuid           int
	name           string
	allocatedSpace Alloc
	Data           string
}

type Alloc struct {
	clusterId  int
	placesName []int
	placesData []int
}

type Cluster struct {
	IsLocked bool
	uuid     int
	Data     []byte
}

type WritingData struct {
	name []byte
	data []byte
}
