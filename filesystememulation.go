package main

import (
	"errors"
	"fmt"
)

func main() {
	filesys := CreateSystem()
	fmt.Printf("initiated filesystem have %s clusters\n", intToStr(len(filesys.Clusters)))
	filesys = CreateFile(filesys, "bob", "asdxzc")
	filesys = CreateFile(filesys, "dirk", "dsaewq")
	filesys, err := Rename(filesys, "bob", "drop")
	CheckErr(err)
	PrintFiles(filesys)
	filesys, err = Delete(filesys, "drop")
	CheckErr(err)
	filesys, err = Copy(filesys, "dirk")
	PrintFiles(filesys)
	filesys, err = mv(filesys, "dirk", 1)
	CheckErr(err)
	PrintFiles(filesys)
	ShowClusterInfo(filesys, 0)
}

func LockCluster(fs FileSystem, clusterId int) (FileSystem, error) {
	if len(fs.Clusters)-1 < clusterId {
		return fs, errors.New("wrong cluster id")
	}
	if fs.Clusters[clusterId].IsLocked {
		fs.Clusters[clusterId].IsLocked = false
	} else {
		fs.Clusters[clusterId].IsLocked = true
	}
	return fs, nil
}

func ShowClusterInfo(fs FileSystem, clusterId int) {
	frees, _ := FindFreeInCluster(fs.Clusters[clusterId], 0)
	fmt.Printf("\nCluster %s have %s free fragments\n	Is locked=%s", clusterId, len(frees), fs.Clusters[clusterId].IsLocked)

}

func Copy(fs FileSystem, name string) (FileSystem, error) {
	file := File{name: ""}
	for _, fileOne := range fs.File {
		if fileOne.name == name {
			file = fileOne
		}
	}
	if file.name == "" {
		return fs, errors.New("no match for filename")
	}
	fs = CreateFile(fs, file.name, file.Data)

	return fs, nil
}

func Delete(fs FileSystem, name string) (FileSystem, error) {
	file := File{name: ""}
	for i, fileOne := range fs.File {
		if fileOne.name == name {
			file = fileOne
			if i == 0 {
				fs.File = append(fs.File[1:])
			} else if i == len(fs.File)-1 {
				fs.File = append(fs.File[:i])
			} else {
				fs.File = append(fs.File[:i], fs.File[:i+1]...)
			}
		}
	}
	if file.name == "" {
		return fs, errors.New("no match for filename")
	}
	fs = WipeData(fs, file.allocatedSpace.clusterId, file.allocatedSpace.placesName)
	fs = WipeData(fs, file.allocatedSpace.clusterId, file.allocatedSpace.placesData)

	return fs, nil
}

func PrintFiles(fs FileSystem) {
	for _, file := range fs.File {
		fmt.Printf("\nuuid:%s  File: %s with data:%s", file.uuid, file.name, file.Data)
		fmt.Printf("\n	This file is located in cluster %s", file.allocatedSpace.clusterId)
	}

}

func mv(fs FileSystem, name string, clusterTo int) (FileSystem, error) {
	file := File{name: ""}
	for i, fileOne := range fs.File {
		if fileOne.name == name {
			file = fileOne
			// check free space
			_, err := FindFreeInCluster(fs.Clusters[clusterTo], len(file.allocatedSpace.placesName)+len(file.allocatedSpace.placesData))
			if (err != nil) || (fs.Clusters[clusterTo].IsLocked) {
				return fs, errors.New("this cluster dont have enough space for move")
			}
			fs.File[i].allocatedSpace.clusterId = clusterTo
			break
		}
	}
	if file.name == "" {
		return fs, errors.New("\nno match for file")
	}

	fs = WipeData(fs, file.allocatedSpace.clusterId, file.allocatedSpace.placesName)
	fs = WipeData(fs, file.allocatedSpace.clusterId, file.allocatedSpace.placesData)

	fs = WriteThisCluster(fs, file, clusterTo)

	return fs, nil
}

func WriteThisCluster(fs FileSystem, file File, clusterId int) FileSystem {
	if fs.Clusters[clusterId].IsLocked {
		fmt.Println("cant write or modify this cluster. int locked")
		return fs
	}
	frees, _ := FindFreeInCluster(fs.Clusters[clusterId], len(file.allocatedSpace.placesName)+len(file.allocatedSpace.placesData))
	filename := []byte(file.name)
	filedata := []byte(file.Data)
	for _, fragment := range filename {
		fs.Clusters[clusterId].Data[frees[0]] = fragment
		frees = append(frees[1:])
	}
	for _, fragment := range filedata {
		fs.Clusters[clusterId].Data[frees[0]] = fragment
		frees = append(frees[1:])
	}
	return fs
}

func Rename(fs FileSystem, oldName string, newName string) (FileSystem, error) {
	// finding file match
	file := File{name: ""}
	for i, fileOne := range fs.File {
		if fileOne.name == oldName {
			file = File{
				name:           newName,
				allocatedSpace: fileOne.allocatedSpace,
				Data:           fileOne.Data,
			}
			if i == 0 {
				fs.File = append(fs.File[i+1:])
			} else {
				fs.File = append(fs.File[:i], fs.File[:i+1]...)
			}

			break
		}
	}
	if file.name == "" {
		return fs, errors.New("\nno match for file")
	}
	// wipe name && data from cluster
	fs = WipeData(fs, file.allocatedSpace.clusterId, file.allocatedSpace.placesName)
	fs = WipeData(fs, file.allocatedSpace.clusterId, file.allocatedSpace.placesData)

	// create renamed file
	fs = CreateFile(fs, file.name, file.Data)
	return fs, nil
}

func WipeData(fs FileSystem, clusterId int, indexes []int) FileSystem {
	for i, _ := range fs.Clusters[clusterId].Data {
		for j, data := range indexes {
			if i == data {
				fs.Clusters[clusterId].Data[i] = 0
				fmt.Printf("\nwiped element %s from cluster %s", data, clusterId)
				indexes = append(indexes[:j], indexes[j:]...)
			}
		}
	}
	return fs
}

func CreateSystem() FileSystem {
	filesys := FileSystem{}
	for i := 0; i < 64; i++ {
		cluster := Cluster{
			Data: make([]byte, 512),
			uuid: i,
		}
		filesys.Clusters = append(filesys.Clusters, cluster)
	}
	return filesys
}

func CreateFile(fs FileSystem, name string, data string) FileSystem {
	fmt.Printf("name in bytes: %s\n", []byte(name))
	fmt.Printf("data in bytes: %s\n", []byte(data))
	fullData := WritingData{
		data: []byte(data),
		name: []byte(name),
	}

	return WriteIn(fs, fullData)
}

func WriteIn(fs FileSystem, data WritingData) FileSystem {
	file := File{}
	if len(fs.File) != 0 {
		file.uuid = fs.File[len(fs.File)-1].uuid + 1
	} else {
		file.uuid = 0
	}

	file.name = string(data.name)
	file.Data = string(data.data)
	for i, cluster := range fs.Clusters {
		frees, err := FindFreeInCluster(cluster, len(data.name)+len(data.data))
		if err != nil {
			fmt.Println(err, intToStr(i))
		} else if cluster.IsLocked {
			fmt.Println("cluster if locked. cant write/change data")
			return fs
		} else {
			file.allocatedSpace.clusterId = i

			// writing name
			for j, fragment := range data.name {
				fs.Clusters[i].Data[frees[j]] = fragment
				file.allocatedSpace.placesName = append(file.allocatedSpace.placesName, frees[j])
				fmt.Printf("\n writing %s in cluster %s with index %s", fragment, cluster.uuid, frees[j])
				// clear used elements from slice
				frees = append(frees[:j], frees[j+1:]...)
			}
			for j, fragment := range data.data {
				if cluster.Data[frees[j]] == 0 {
					fs.Clusters[i].Data[frees[j]] = fragment
					file.allocatedSpace.placesData = append(file.allocatedSpace.placesData, frees[j])
					fmt.Printf("\n writing %s in cluster %s with index %s", fragment, cluster.uuid, frees[j])
					frees = append(frees[:j], frees[j+1:]...)
				}
			}
			fs.File = append(fs.File, file)
			//break
			return fs
		}
	}
	return fs
}

func FindFreeInCluster(cluster Cluster, target int) ([]int, error) {
	freeIndexes := []int{}
	for i, data := range cluster.Data {
		if data == 0 {
			freeIndexes = append(freeIndexes, i)
		}
	}
	if len(freeIndexes) >= target {
		return freeIndexes, nil
	} else {
		return freeIndexes, errors.New("not sufficient space in cluster")
	}
}
