package main

import (
	"fmt"
	"strconv"
)

// переводы данных

func intToStr(num int) string {
	return strconv.Itoa(num)
}

func strToiInt(str string) int {
	i, e := strconv.Atoi(str)
	CheckErr(e)
	return i
}

func uint8ToStr(num uint8) string {
	i := strconv.FormatUint(uint64(num), 10)
	return i
}

func CheckErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
